package com.example.apiejemplo.controller;

import com.example.apiejemplo.domain.model.Sale;
import com.example.apiejemplo.domain.model.SaleDetail;
import com.example.apiejemplo.domain.service.SaleService;
import com.example.apiejemplo.request.SaleRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/sales")
public class SaleController {
    @Autowired
    private SaleService saleService;

    @GetMapping()
    public List<Sale> getAllSalesWithDetails() {
        return saleService.getAllSalesWithDetails();
    }

    @PostMapping("")
    public ResponseEntity<String> createSale(@RequestBody SaleRequest request) {
        saleService.saveSale(request);
        return ResponseEntity.ok("Sale saved successfully");
    }
}
