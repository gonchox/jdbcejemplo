package com.example.apiejemplo.controller;

import com.example.apiejemplo.domain.model.Client;
import com.example.apiejemplo.domain.service.ClientService;
import com.example.apiejemplo.request.ClientRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/clients")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @GetMapping("")
    public List<Client> getAllClients() {
        return clientService.getAllClients();
    }

    @GetMapping("/{id}")
    public Client getClientById(@PathVariable int id) {
        return clientService.getClientById(id);
    }

    @PostMapping("")
    public ResponseEntity<String> createClient(@RequestBody ClientRequest request) {
        try {
            clientService.createClient(request);
            return ResponseEntity.ok("Client saved successfully");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to save client");
        }
    }

}
