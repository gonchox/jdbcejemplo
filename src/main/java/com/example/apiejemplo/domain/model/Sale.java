package com.example.apiejemplo.domain.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Sale {
    private Integer id;
    private Integer clientId;
    private double total;

    private List<SaleDetail> saleDetailList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<SaleDetail> getSaleDetailList() {
        return saleDetailList;
    }

    public void setSaleDetailList(List<SaleDetail> saleDetailList) {
        this.saleDetailList = saleDetailList;
    }
}
