package com.example.apiejemplo.domain.service;

import com.example.apiejemplo.domain.model.Client;
import com.example.apiejemplo.domain.model.Sale;
import com.example.apiejemplo.domain.model.SaleDetail;
import com.example.apiejemplo.domain.repository.ISaleRepository;
import com.example.apiejemplo.request.SaleRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleService {

    @Autowired
    private ISaleRepository saleRepository;

    public List<Sale> getAllSalesWithDetails() {
        return saleRepository.getAllSalesWithDetails();
    }

    public void saveSale(SaleRequest request) {
        Sale sale = new Sale();
        sale.setTotal(request.getTotal());
        sale.setClientId(request.getClientId());
        saleRepository.saveSale(request);
    }
}
