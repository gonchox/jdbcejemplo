package com.example.apiejemplo.domain.service;

import com.example.apiejemplo.domain.model.Product;
import com.example.apiejemplo.domain.repository.IProductRepository;
import com.example.apiejemplo.request.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private IProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(int id) {
        return productRepository.findById(id);
    }

    public void createProduct(ProductRequest request) {
        Product product = new Product();
        product.setDescription(request.getDescription());
        product.setCost(request.getCost());
        productRepository.save(request);
    }
}
