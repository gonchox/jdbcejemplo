package com.example.apiejemplo.domain.service;

import com.example.apiejemplo.domain.model.Client;
import com.example.apiejemplo.domain.repository.IClientRepository;
import com.example.apiejemplo.request.ClientRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
   @Autowired
   private IClientRepository clientRepository;

    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    public Client getClientById(int id) {
        return clientRepository.findById(id);
    }

    public void createClient(ClientRequest request) {
        Client client = new Client();
        client.setName(request.getName());
        client.setPhone(request.getPhone());
        clientRepository.save(request);
    }

}
