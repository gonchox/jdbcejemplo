package com.example.apiejemplo.domain.repository;

import com.example.apiejemplo.domain.model.Client;
import com.example.apiejemplo.domain.model.Product;
import com.example.apiejemplo.request.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class IProductRepository {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Product> findAll() {
        return jdbcTemplate.query("CALL sp_get_all_products()", new IProductRepository.ProductMapper());
    }

    public Product findById(int id) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.queryForObject("CALL sp_get_product_by_id(:id)", parameters, new IProductRepository.ProductMapper());
    }

    public void save(ProductRequest request) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("description", request.getDescription())
                .addValue("cost", request.getCost());
        jdbcTemplate.update("CALL sp_create_product(:description, :cost)", parameters);
    }

    private static class ProductMapper implements RowMapper<Product> {
        @Override
        public Product mapRow(ResultSet resultSet, int i) throws SQLException {
            Product product = new Product();
            product.setId(resultSet.getInt("id"));
            product.setDescription(resultSet.getString("description"));
            product.setCost(resultSet.getDouble("cost"));
            return product;
        }
    }
}
