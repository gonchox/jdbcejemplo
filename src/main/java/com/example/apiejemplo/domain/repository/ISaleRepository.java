package com.example.apiejemplo.domain.repository;

import com.example.apiejemplo.domain.model.Sale;
import com.example.apiejemplo.domain.model.SaleDetail;
import com.example.apiejemplo.request.SaleDetailRequest;
import com.example.apiejemplo.request.SaleRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ISaleRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Sale> getAllSalesWithDetails() {
        String query = "SELECT s.id, s.client_id, s.total, sd.product_id, sd.quantity, sd.subtotal " +
                "FROM sales s JOIN sale_details sd ON s.id = sd.sale_id";



        List<Sale> sales = new ArrayList<>();
        Map<Integer, Sale> saleMap = new HashMap<>();

        jdbcTemplate.query(query, rs -> {
            Integer saleId = rs.getInt("id");
            Sale sale = saleMap.get(saleId);

            if (sale == null) {
                // crea un nuevo objeto Sale si no existe ya
                sale = new Sale();
                sale.setId(saleId);
                sale.setClientId(rs.getInt("client_id"));
                sale.setTotal(rs.getDouble("total"));
                sale.setSaleDetailList(new ArrayList<>());
                saleMap.put(saleId, sale);
                sales.add(sale);
            }

            // añade un objeto details a Sale
            SaleDetail saleDetail = new SaleDetail();
            saleDetail.setId(rs.getInt("id"));
            saleDetail.setSaleId(saleId);
            saleDetail.setProductId(rs.getInt("product_id"));
            saleDetail.setQuantity(rs.getInt("quantity"));
            saleDetail.setSubtotal(rs.getBigDecimal("subtotal"));
            sale.getSaleDetailList().add(saleDetail);
        });

        return sales;
    }
    public void saveSale(SaleRequest request) {
        // Guarda el Sale
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO sales (client_id, total) VALUES (?, ?)";
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, request.getClientId());
            ps.setDouble(2, request.getTotal());
            return ps;
        }, keyHolder);

        // Conseguimos el nuevo id creado
        int saleId = keyHolder.getKey().intValue();

        // Guardamos SaleDetails
        for (SaleDetailRequest detailRequest : request.getSaleDetails()) {
            String detailSql = "INSERT INTO sale_details (sale_id, product_id, quantity, subtotal) VALUES (?, ?, ?, ?)";
            KeyHolder detailKeyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(detailSql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, saleId);
                ps.setInt(2, detailRequest.getIdProduct());
                ps.setInt(3, detailRequest.getQuantity());
                double subtotal = detailRequest.getQuantity() * request.getTotal();
                ps.setDouble(4, subtotal);
                return ps;
            }, detailKeyHolder);

            // Guardamos el nuevo id de SaleDetail
            int saleDetailId = detailKeyHolder.getKey().intValue();
            detailRequest.setId(saleDetailId);
        }

        request.setId(saleId);
    }
    }
