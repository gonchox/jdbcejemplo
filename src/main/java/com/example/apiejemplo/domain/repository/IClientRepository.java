package com.example.apiejemplo.domain.repository;

import com.example.apiejemplo.domain.model.Client;
import com.example.apiejemplo.request.ClientRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class IClientRepository {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Client> findAll() {
        return jdbcTemplate.query("CALL sp_get_all_clients()", new ClientMapper());
    }

    public Client findById(int id) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.queryForObject("CALL sp_get_client_by_id(:id)", parameters, new ClientMapper());
    }

    public void save(ClientRequest request) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name", request.getName())
                .addValue("phone", request.getPhone());
        jdbcTemplate.update("CALL sp_create_client(:name, :phone)", parameters);
    }


    private static class ClientMapper implements RowMapper<Client> {
        @Override
        public Client mapRow(ResultSet resultSet, int i) throws SQLException {
            Client client = new Client();
            client.setId(resultSet.getInt("id"));
            client.setName(resultSet.getString("name"));
            client.setPhone(resultSet.getString("phone"));
            return client;
        }
    }
}
