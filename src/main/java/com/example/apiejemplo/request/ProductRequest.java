package com.example.apiejemplo.request;

import lombok.Data;

@Data
public class ProductRequest {
    private String description;
    private double cost;
}
