package com.example.apiejemplo.request;

import lombok.Data;

@Data
public class ClientRequest {
    private String name;
    private String phone;
}
