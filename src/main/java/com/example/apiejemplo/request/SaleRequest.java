package com.example.apiejemplo.request;

import com.example.apiejemplo.domain.model.SaleDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class SaleRequest {
    private Integer id;
    private Integer clientId;
    private double total;
    private List<SaleDetailRequest> saleDetails;

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<SaleDetailRequest> getSaleDetails() {
        return saleDetails;
    }

    public void setSaleDetails(List<SaleDetailRequest> saleDetails) {
        this.saleDetails = saleDetails;
    }
}
